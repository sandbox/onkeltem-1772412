<?php

/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_event_info().
 */
function feeds_rules_after_save_rules_event_info() {
  $info = array();
  $entity_info = entity_get_info();

  foreach (feeds_importer_load_all() as $importer) {
    $config = $importer->getConfig();
    $processor = feeds_plugin($config['processor']['plugin_key'], $importer->id);

    // It's possible to get FeedsMissingPlugin here which will break things
    // since it doesn't implement FeedsProcessor::entityType().
    if (!$processor instanceof FeedsProcessor) {
      continue;
    }

    $entity_type = $processor->entityType();
    $label = isset($entity_info[$entity_type]['label']) ? $entity_info[$entity_type]['label'] : $entity_type;

    $info['feeds_rules_after_save_'. $importer->id] = array(
      'label' => t('After saving an item imported via @name.', array('@name' => $importer->config['name'])),
      'group' => t('Feeds'),
      'variables' => array(
        $entity_type => array(
          'label' => t('Imported @label', array('@label' => $label)),
          'type' => $entity_type,
        ),
      ),
      'access callback' => 'feeds_rules_access_callback',
    );
    // Add bundle information if the node processor is used.
    if ($processor instanceof FeedsNodeProcessor) {
      $config = $processor->getConfig();
      $info['feeds_rules_after_save_'. $importer->id]['variables'][$entity_type]['bundle'] = $config['content_type'];
    }
  }
  return $info;
}
